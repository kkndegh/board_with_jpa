package com.spring.board.config;

import com.zaxxer.hikari.HikariDataSource;
import lombok.Getter;
import lombok.Setter;
import net.sf.log4jdbc.Log4jdbcProxyDataSource;
import net.sf.log4jdbc.tools.Log4JdbcCustomFormatter;
import net.sf.log4jdbc.tools.LoggingType;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

/**
 * 데이터베이스 연결 설정
 *
 * @since 2021-11-25
 */
public abstract class DatabaseConfiguration {

}

/**
 * 기본 데이터베이스 연결 설정
 *
 * @since 2021-11-25
 */
@Configuration
@EnableTransactionManagement
@Getter
@Setter
@ConfigurationProperties(prefix = "db.datasource")
class DefaultDatabaseConfiguration extends DatabaseConfiguration {

  private String url;
  private String type;
  private String driverClassName;
  private String username;
  private String password;
  private String schema;

  /**
   * @Primary: 같은 우선순위로 있는 클래스가 여러개가 있을 시 그 중 가장 우선순위로 주입할 클래스 타입을 선택
   */
  @Primary
  @Bean(name = "defaultDataSource")
  public DataSource dataSource() {
    HikariDataSource hikariDataSource = new HikariDataSource();
    hikariDataSource.setJdbcUrl(url);
    hikariDataSource.setDriverClassName(driverClassName);
    hikariDataSource.setUsername(username);
    hikariDataSource.setPassword(password);
    hikariDataSource.setSchema(schema);

    //sql 출력 format
    Log4jdbcProxyDataSource logDataSource = new Log4jdbcProxyDataSource(hikariDataSource);

    Log4JdbcCustomFormatter formatter = new Log4JdbcCustomFormatter();
    formatter.setLoggingType(LoggingType.MULTI_LINE);
    logDataSource.setLogFormatter(formatter);
    return logDataSource;
  }

}