package com.spring.board.user.service;

import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Optional;

import com.spring.board.user.domain.User;
import com.spring.board.user.repository.UserRepository;

import org.springframework.stereotype.Service;

/**
 * BOARD REST API 서비스
 * 
 * @author kh
 * @since 2023.02.28
 */
@RequiredArgsConstructor
@Service
public class UserService {

    private final UserRepository userRepository;

    public List<User> findByAll() {
        return userRepository.findAll();
    }

    public User findById(Long id) {
        return userRepository.findById(id).orElse(null);
    }

    public void save(User user) {
        userRepository.save(user);
    }

    public void deleteById(Long id) {
        userRepository.deleteById(id);
    }

    public Optional<User> findByUserInfo(User user) {
        return userRepository.findByEmail(user.getEmail());
    }
}