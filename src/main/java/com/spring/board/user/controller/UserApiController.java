package com.spring.board.user.controller;

import java.util.List;
import java.util.Optional;

import com.google.gson.Gson;
import com.spring.board.common.dto.ResultDto;
import com.spring.board.common.eenum.CodeEnum;
import com.spring.board.user.domain.User;
import com.spring.board.user.service.UserService;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * USER API 컨트롤러
 * 
 * @author kh
 * @since 2023.02.28
 */
@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/user")
public class UserApiController {

    private final UserService userService;
    
    //헬스체크
    @GetMapping("/healthcheck")
    public ResponseEntity<?> sayHello() {
        Gson gson = new Gson();
        String rtnStr = "{}";

        ResultDto resultDto = ResultDto.builder()
                            .rstCode(CodeEnum.CON_SUCCESS.getCode())
                            .rstMsg(CodeEnum.CON_SUCCESS.getMessage())
                            .data("{rtn : '헬스체크'}")
                            .build();
        rtnStr = gson.toJson(resultDto);

        return new ResponseEntity<>(rtnStr, HttpStatus.OK);
    }

    //전체 회원조회
    @GetMapping("/get")
    public ResponseEntity<String> getAllUser() {
        Gson gson = new Gson();
        String rtnStr = "{}";

        try {
            List<User> user = userService.findByAll();

            ResultDto resultDto = ResultDto.builder()
                                .rstCode(CodeEnum.CON_SUCCESS.getCode())
                                .rstMsg(CodeEnum.CON_SUCCESS.getMessage())
                                .data(gson.toJson(user))
                                .build();
            rtnStr = gson.toJson(resultDto);
        } catch (Exception e) {
            log.error("에러발생 :: " + e.toString());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(rtnStr, HttpStatus.OK);
    }

    //회원정보 조회
    @PostMapping("/get")
    public ResponseEntity<String> getUsers(@RequestBody User user) {
        Gson gson = new Gson();
        String rtnStr = "{}";
        
        try {
            Optional<User> userInfo = userService.findByUserInfo(user);

            ResultDto resultDto = ResultDto.builder()
                                .rstCode(CodeEnum.CON_SUCCESS.getCode())
                                .rstMsg(CodeEnum.CON_SUCCESS.getMessage())
                                .data(gson.toJson(userInfo))
                                .build();
            rtnStr = gson.toJson(resultDto);
        } catch (Exception e) {
            log.error("에러발생 :: " + e.toString());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(rtnStr, HttpStatus.OK);
    }

    //회원조회
    @GetMapping("/get/{id}")
    public ResponseEntity<String> getUser(@PathVariable Long id) {
        Gson gson = new Gson();
        String rtnStr = "{}";

        try {
            User user = userService.findById(id);

            ResultDto resultDto = ResultDto.builder()
                                .rstCode(CodeEnum.CON_SUCCESS.getCode())
                                .rstMsg(CodeEnum.CON_SUCCESS.getMessage())
                                .data(gson.toJson(user))
                                .build();
            rtnStr = gson.toJson(resultDto);
        } catch (Exception e) {
            log.error("에러발생 :: " + e.toString());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(rtnStr, HttpStatus.OK);
    }

    //회원가입
    @PostMapping("/add")
    public ResponseEntity<String> addUser(@RequestBody User user) {
        Gson gson = new Gson();
        String rtnStr = "{}";

        try {
            if(userService.findByUserInfo(user).isPresent()){
                ResultDto resultDto = ResultDto.builder()
                                .rstCode(CodeEnum.FAIL_LOGIN.getCode())
                                .rstMsg(CodeEnum.FAIL_LOGIN.getMessage())
                                .build();
                rtnStr = gson.toJson(resultDto);
            }else{
                userService.save(user);

                ResultDto resultDto = ResultDto.builder()
                                .rstCode(CodeEnum.CON_SUCCESS.getCode())
                                .rstMsg(CodeEnum.CON_SUCCESS.getMessage())
                                .build();
                rtnStr = gson.toJson(resultDto);
            }
        } catch (Exception e) {
            log.error("에러발생 :: " + e.toString());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(rtnStr, HttpStatus.OK);
    }

    //회원수정
    @PutMapping("/modify")
    public ResponseEntity<String> modifyUser(@RequestBody User user) {
        Gson gson = new Gson();
        String rtnStr = "{}";

        try {
            userService.save(user);

            ResultDto resultDto = ResultDto.builder()
                            .rstCode(CodeEnum.CON_SUCCESS.getCode())
                            .rstMsg(CodeEnum.CON_SUCCESS.getMessage())
                            .build();
            rtnStr = gson.toJson(resultDto);
        } catch (Exception e) {
            log.error("에러발생 :: " + e.toString());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(rtnStr, HttpStatus.OK);
    }

    //회원삭제
    @DeleteMapping("/del/{id}")
    public ResponseEntity<String> delUser(@PathVariable Long id) {
        Gson gson = new Gson();
        String rtnStr = "{}";

        try {
            userService.deleteById(id);

            ResultDto resultDto = ResultDto.builder()
                            .rstCode(CodeEnum.CON_SUCCESS.getCode())
                            .rstMsg(CodeEnum.CON_SUCCESS.getMessage())
                            .build();
            rtnStr = gson.toJson(resultDto);
        } catch (Exception e) {
            log.error("에러발생 :: " + e.toString());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(rtnStr, HttpStatus.OK);
    }
}