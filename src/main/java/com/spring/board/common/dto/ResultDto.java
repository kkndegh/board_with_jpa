package com.spring.board.common.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ResultDto {
    
    private int rstCode;
    private String rstMsg;
    private String data;
}