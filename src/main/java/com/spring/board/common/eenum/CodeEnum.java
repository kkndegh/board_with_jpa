package com.spring.board.common.eenum;

public enum CodeEnum {
    CON_SUCCESS(200, "SUCCESS."),
    INVALID_INPUT(400, "입력값이 올바르지 않습니다."),
    NOT_FOUND(404, "요청한 리소스를 찾을 수 없습니다."),
    INTERNAL_SERVER_ERROR(500, "서버 내부 오류가 발생하였습니다."),

    FAIL_LOGIN(400, "회원가입 실패.");

    private final int code;
    private final String message;

    CodeEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
