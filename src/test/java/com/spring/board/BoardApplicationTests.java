package com.spring.board;

import com.spring.board.user.domain.User;
import com.spring.board.user.repository.UserRepository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class BoardApplicationTests {

    @Autowired
    private UserRepository userRepository;

    @Test
    void save() {

        // 1. 게시글 파라미터 생성
        User params = User.builder()
                .age(21)
                .name("도뎡이")
                .build();

        // 2. 게시글 저장
        userRepository.save(params);

        // 3. 1번 게시글 정보 조회
        User entity = userRepository.findById((long) 1).get();
        assertThat(entity.getAge()).isEqualTo(21);
        assertThat(entity.getName()).isEqualTo("도뎡이");
    }

}
